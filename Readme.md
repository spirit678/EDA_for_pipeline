# EDA for lncrna_conservation_nf

Here we present Exploratory data analysis for results obtained by using our computational pipeline https://gitlab.com/spirit678/lncrna_conservation_nf
The report consist of four main chapters:
* Chapter One is dedicated to non-coding transcriptomes of human and mouse
* Chapter Two is dedicated to conservation study.
* Chapter Three is focused on the comparison of obtained datasets
* Chapter Four focused on checking some of well known lncRNA

Also we provide two sweetviz reports with comparison of the datasets
